#!/usr/bin/python3
# -*- coding: utf-8 -*-

import psycopg2
import secrets
import re
from passlib.hash import sha512_crypt
import getpass

# def gen_password(length=18, charset="ACDEFHJKMNPQRTUVWXYZacdefghjkmnpqrstuvwxyz23479!@#%^&*()"):
#     password=("".join([secrets.choice(charset) for _ in range(0, length)]))
#     return (password)


import random
import string

def gen_password():
    # choose from all lowercase letter
    letters = "ACDEFHJKMNPQRTUVWXYZacdefghjkmnpqrstuvwxyz23479!@#%^&*()"
    result_str = '123456'.join(random.choice(letters) for i in range(18))
    print("Random string of length", 18, "is:", result_str)
    return (result_str)



def gen_password_hash():
    # password_hash=(sha512_crypt.using(rounds=5000).hash((gen_password())))
    # password_hash=(sha512_crypt.using(rounds=5000).hash(("asas")))
    password_hash = sha512_crypt.using(salt='vQ4zXTBQ`A{!6f\.b,', rounds=5000).hash('123456')
    print (password_hash)
    return (password_hash)



gen_password_hash()


# gen_password_hash()