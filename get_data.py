#!/usr/bin/python3
# -*- coding: utf-8 -*-

import psycopg2
import re
from passlib.hash import sha512_crypt
import getpass
import random
# import secrets
# import string

def is_empty_or_blank():
    return re.search("^\s*$")

# Генерируем пароль
def gen_password():    
    letters = "ACDEFHJKMNPQRTUVWXYZacdefghjkmnpqrstuvwxyz23479!@#%^&*()"
    result_str = ''.join(random.choice(letters) for i in range(18))
    # print("Random string of length", 18, "is:", result_str)
    return (result_str)

# Генерируем хэш
def gen_password_hash(stored_password):
    password_hash=sha512_crypt.using(salt='GLrSn2bEAGx', rounds=5000).hash(stored_password)
    print (password_hash)
    return (password_hash)


conn = psycopg2.connect(dbname='ansible-users', user='postgres', host='localhost')



vars_file = '/usr/local/ansible-scripts/users-groups/linux_server/vars/main.yml'

task_users_file = '/usr/local/ansible-scripts/users-groups/linux_server/tasks/users.yml'

ansible_vars = open(vars_file, "a")

ansible_task_users = open(task_users_file, "a")


# Очищаем файл c списком пользователей от данных
open(vars_file, 'w').close()


# Очищаем файл c списком параметров пользоваетелей от данных
open(task_users_file, 'w').close()

def password_insert(linux_username):
    with conn.cursor() as cur:
        query=("UPDATE linux_users SET password='" + gen_password() + "' WHERE linux_username='" + linux_username + "'")
        cur.execute(query)
        conn.commit()        
    return ()


def password_hash_insert(linux_username, generated_password):
    with conn.cursor() as cur:
        query=("UPDATE linux_users SET linux_user_password_hash='" + generated_password + "' WHERE linux_username='" + linux_username + "'")
        cur.execute(query)
        conn.commit()        
    return ()








def users_groups_list():

    # Open a cursor to perform database operations
    with conn.cursor() as cur:
        
        # Create groups
        cur.execute('SELECT "linux_groupname" FROM groups')
       
        ansible_vars.writelines(["---", "\n", "# vars file for common", "\n", "\n", "linux_groups:", "\n"])
        for group in cur:
            group = "  - " + (str(group)[2:-3])
            # print (group)
            ansible_vars.write(group + "\n")


        # Create users
        cur.execute('SELECT "linux_username" FROM linux_users')
        ansible_vars.writelines(["\n", "\n", "linux_users:", "\n"])
        for user in cur:
            user = "  - " + (str(user)[2:-3])
            # print (user)
            ansible_vars.write(user + "\n")

        # Create password for users without password
        pass_check_query = ('SELECT "linux_username", "password" FROM linux_users')
        cur.execute(pass_check_query)
        for user_data in cur:
            # print (user_data)
            password_check = str(user_data[1])
            if(len(password_check) == 0) or password_check == 'None':
                # print("password is empty")
                password_insert(user_data[0])
            else:
                None

        
        # Create password-hash for users without password-hash
        pass_hash_check_query = ('SELECT "linux_username", "password", "linux_user_password_hash" FROM linux_users')
        cur.execute(pass_hash_check_query)
        for user_data in cur:
            password_hash_check = str(user_data[2])
            if(len(password_hash_check) == 0) or password_hash_check == 'None' or gen_password_hash(user_data[1]) != user_data[2]:
                # print("password is empty")
                generated_password = gen_password_hash(user_data[1])
                password_hash_insert(user_data[0], generated_password)
            else:
                None
                

        # Create users with settings
        # query = str('SELECT "linux_username", "password", "groups", "lock settings", "sudoers settings" FROM public.linux_users')
        ansible_task_users.write("---" + "\n" + " # tasks file for creation users" + "\n" + "\n" + "\n")
        query = str('SELECT "linux_username", "password", "groups", "lock settings", "state", "linux_user_password_hash", "hosts" FROM linux_users')
        cur.execute(query)
        for user_data in cur:
            # print (user_data)
            # print (user_data[0])
            ansible_task_users.write("- name: Add user account " + str(user_data[0]) + "\n")
            ansible_task_users.write("  become: true" + "\n")
            
            ansible_task_users.write("  user: " + "\n")
            ansible_task_users.write("    name: " + user_data[0]+ "\n") 
            ansible_task_users.write("    state: " + user_data[4] + "\n")
         
            ansible_task_users.write('    password: ' + "'" + user_data[5] + "'" + "\n")
            ansible_task_users.write("    groups: "  + user_data[2]  + "\n")
            if user_data[3] == 'true':
                ansible_task_users.write("    shell: /sbin/nologin" + "\n")
            else:
                ansible_task_users.write("    shell: /bin/bash" + "\n")
            # ansible_task_users.write("    password_lock: " + user_data[3] + "\n")
            hosts = user_data[6].split(",")
            ansible_task_users.write("  when: inventory_hostname in " +  str(hosts) + "\n")
            # ansible_task_users.write("  when: inventory_hostname in ['someserver']" + "\n")
            ansible_task_users.write("\n"+ "\n")


        # Make the changes to the database persistent
        conn.commit()
        
    return ()




users_groups_list()