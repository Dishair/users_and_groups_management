#!/usr/bin/env bash
cd /usr/local/ansible-scripts/
git reset --hard && git clean -fd
git pull
python3 /usr/local/ansible-scripts/users-groups/test.py